package com.example.demo.ocr.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.common.OcrUtils;
import com.example.demo.config.Constant;
import com.example.demo.ocr.vo.OcrAddrVo;
import com.example.demo.ocr.vo.OcrCiVo;
import com.example.demo.ocr.vo.OcrJuminChobonVo;
import com.example.demo.ocr.vo.OcrPageVo;
import com.example.demo.ocr.vo.OcrWordCiVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 주민등록초본 정보 추출
 * @version 1.0
 * @author lcg
 */
@Slf4j
@Service
public class OcrJuminChobonService {

    @Autowired
	private OcrJsonParserService ocrJsonParserService;

    // 표준 날짜 형식
    final String dateFormat = "yyyy-MM-dd";
    final SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

    // 좌표지정 텍스트
    final String APPLICANT_NAME_TXT = "성명(한자)";
    final String USE_PURPOSE_TXT = "용도및목적:";
    final String NUMBER_TXT = "번호";
    final String CHANGE_RESON_TXT = "변동사유";
    final String REG_STATE_TXT = "등록상태";

    // 주소구분 텍스트
    final String GYEONGGI_TXT = "경기도";
    // OCR 이미지 오차범위
    final int ERROR_RANGE = 20;

     /**
     * 초본 파일 OCR 데이터 
     * 
     * @param String
     * @return Map<String,Object>
     * @throws Exception
     */
    public Map<String,Object> getChoBon(String fileName) throws Exception {
        log.debug("#####getChoBon fileName = " + fileName);
        // JSON 파일에서 OCR 추출 테이터 가져오기
        JSONArray images = ocrJsonParserService.convertFileToJson(Constant.JUMIN_CHOBON, fileName);
        return analyzeChoBon(images);

    }   

    /**
     * 초본 추출 데이터 - 발급일, 성명, 주민번호, 생년월일, 거주 주소 이력, 마지막 거주 주소
     * , 경기도 연속 거주일수, 경기도 합산 거주일수
     * 
     * @param String
     * @return Map<String,Long>
     * @throws Exception
     */
    public Map<String,Object> analyzeChoBon(JSONArray images) throws Exception {
        Map<String,Object> result = new HashMap<String,Object>();

        OcrJuminChobonVo ojc = new OcrJuminChobonVo();
        ojc.setBsnsLocationName(GYEONGGI_TXT);
        
		// '용도및목적' 좌표
		OcrWordCiVo uptCi = new OcrWordCiVo(USE_PURPOSE_TXT);

		// '성명(한자)' 좌표
		OcrWordCiVo anmCi = new OcrWordCiVo(APPLICANT_NAME_TXT);

        // 발행일 추출 임시
        String tempIssueDt = "";
        OcrCiVo tempIssueDtCi = new OcrCiVo();

		for(int i=0; i < images.size(); i++){

            JSONObject image = (JSONObject)images.get(i);

            // 주소 > '번호' 좌표
            OcrWordCiVo numCi = new OcrWordCiVo(NUMBER_TXT);

            // 주소 > '변동사유' 좌표
            OcrWordCiVo acrCi = new OcrWordCiVo(CHANGE_RESON_TXT);

            // 주소 > '등록상태' 좌표
            OcrWordCiVo rstCi = new OcrWordCiVo(REG_STATE_TXT);

            OcrPageVo opv = new OcrPageVo();
            opv.setPage(i);

            // 추출데이터 좌표
            List<OcrCiVo> boxList = new ArrayList<>();

            // 주소 데이터 
            List<OcrAddrVo> addrList = new ArrayList<>();

            JSONArray fields = (JSONArray)image.get("fields");
            JSONObject convertedImageInfo = (JSONObject)image.get("convertedImageInfo");
            Long width = (Long)convertedImageInfo.get("width");
            Long height = (Long)convertedImageInfo.get("height");
            opv.setFileWidth(width.intValue());
            opv.setFileHeight(height.intValue());

            // 주소 추출 임시
            String tempAddr = "";
            String tempAddrDt = "";
            OcrCiVo tempAddrCi = new OcrCiVo();
            String addrLine = "";
            int addrLineCnt = 0;
            int addrStartXci = 0;
            String changeResonLine = ""; // 변동사유 텍스트 라인
            
            for(int f = 0; f < fields.size(); f++){
                JSONObject field = (JSONObject)fields.get(f);
                // 추출 텍스트 좌표정보
                JSONObject boundingPoly = (JSONObject)field.get("boundingPoly");
                // 추출 텍스트
                String inferText = (String)field.get("inferText");
                // 추출 정확도
                Double inferConfidence = (Double)field.get("inferConfidence");
                // 추출 정확도 특정 포인트 미만은 제외 처리
                if(inferConfidence < 0.7){
                    inferText = "";
                }
                // 추출 텍스트 다음 줄바꿈 정보
                boolean lineBreak = (boolean)field.get("lineBreak");
                // 현재 문자 좌표
                OcrCiVo currentCi = OcrUtils.getCoordinate(boundingPoly);

                // 발행일 
                // 발행일이 없고 용도및목적 ci 확인
                if(ojc.getIssueDt().isEmpty() && uptCi.isCompareEnd()){
                    if(inferText.contains("년") || inferText.contains("월") || inferText.contains("일")){
                        if(tempIssueDt.isEmpty()){
                            tempIssueDtCi = currentCi;
                        }else{
                            tempIssueDtCi.setX2(currentCi.getX2());
                            tempIssueDtCi.setY2(currentCi.getY2());
                            tempIssueDtCi.setX3(currentCi.getX3());
                            tempIssueDtCi.setY3(currentCi.getY3());
                        }
                        tempIssueDt += inferText.replaceAll(" ", "") + " ";
                    }
                    Matcher issueDtMatchYMD = Pattern.compile(OcrUtils.dateRegEx2).matcher(tempIssueDt);
                    if(issueDtMatchYMD.find()){
                        ojc.setIssueDt(issueDtMatchYMD.group());
                        boxList.add(tempIssueDtCi);
                    }
                }

                // 이름
                if(ojc.getIssueNm().isEmpty() && anmCi.isCompareEnd()){
                    boxList.add(currentCi);
                    ojc.setIssueNm(inferText.trim());
                }

                // 주민등록번호
                if(ojc.getJuminRegNo().isEmpty() && anmCi.isCompareEnd()){
                    Matcher juminMatch1 = Pattern.compile(OcrUtils.juminNoRegEx1).matcher(inferText);
                    if(juminMatch1.find()){
                        boxList.add(currentCi);
                        ojc.setJuminRegNo(juminMatch1.group());
                    }
                    Matcher juminMatch2 = Pattern.compile(OcrUtils.juminNoRegEx2).matcher(inferText);
                    if(juminMatch2.find()){
                        boxList.add(currentCi);
                        ojc.setJuminRegNo(juminMatch2.group());
                    }
                }

                // 거주지 주소
                // 번호 영역 - 주소시작 X1 좌표보다 작은 영역
                if(numCi.isCompareEnd() && currentCi.getX2() < addrStartXci){
                    // 번호 영역에 숫자가 아니면 주소 처리 초기화 - 숫자 마지막 라인 
                    Matcher numMatch = Pattern.compile(OcrUtils.numRegEx).matcher(inferText);
                    if(!numMatch.find() && addrLineCnt > 0){
                        // 주소정보 셋팅
                        OcrAddrVo oav = new OcrAddrVo();
                        oav.setAddr(tempAddr);
                        oav.setAddrDate(tempAddrDt);
                        oav.setAddrCi(tempAddrCi);
                        addrList.add(oav);

                        addrLineCnt = 0;
                        addrLine = "";
                        tempAddr = "";
                        tempAddrDt = "";
                    }
                }

                // 주소 영역
                if(acrCi.isCompareEnd() 
                    && currentCi.getX1() > addrStartXci
                    && currentCi.getX2() < acrCi.getWordCi().getX1()){
                    // 시도로 시작시 주소 시작 
                    if(addrLineCnt == 0 && OcrUtils.sidoStartsWith(inferText)){
                        addrLineCnt = 1;
                        addrLine = inferText;
                        tempAddrCi = currentCi;
                        tempAddr = "";
                        tempAddrDt = "";
                        if(addrStartXci == 0){
                            // 주소텍스트 시작점 최초 1회 셋팅
                            addrStartXci = currentCi.getX1() - ERROR_RANGE;
                        }
                    } else if(addrLineCnt > 0) {
                        if(OcrUtils.sidoStartsWith(inferText)){
                            // 주소 취합중 시도가 나오는 경우 주소 라인 재취합
                            if(!tempAddr.isEmpty() && !tempAddrDt.isEmpty()){
                                OcrAddrVo oav = new OcrAddrVo();
                                oav.setAddr(tempAddr);
                                oav.setAddrDate(tempAddrDt);
                                oav.setAddrCi(tempAddrCi);
                                addrList.add(oav);
                            }
                        
                            addrLineCnt = 1;
                            addrLine = inferText;
                            tempAddrCi = currentCi;
                            tempAddr = "";
                            tempAddrDt = "";
                        } else if(inferText.startsWith("[")){
                            // 주소라인 텍스트가 '['로 시작하는 경우 
                            if(!tempAddr.isEmpty() && !tempAddrDt.isEmpty()){
                                OcrAddrVo oav = new OcrAddrVo();
                                oav.setAddr(tempAddr);
                                oav.setAddrDate(tempAddrDt);
                                oav.setAddrCi(tempAddrCi);
                                addrList.add(oav);
                            }

                            addrLineCnt = 0;
                            addrLine = "";
                            tempAddr = "";
                            tempAddrDt = "";
                        } else {
                            // 주소 텍스트 라인 취합 
                            addrLine += " " + inferText;
                            tempAddrCi.setY3(currentCi.getY3());
                            tempAddrCi.setY4(currentCi.getY4());
                        }
                    }
                }

                // 거주지 날짜 영역
                // 등록상태 ci 확인완료, 변동사유 시작점 좌표 이후 텍스트, 주소라인
                if(acrCi.isCompareEnd() // 변동사유 ci 확인완료
                    && rstCi.isCompareEnd() // 변동사유 ci 확인완료
                    && currentCi.getX1() > (acrCi.getWordCi().getX1() + ERROR_RANGE ) // 변동사유 시작점 좌표 이후 텍스트
                    && currentCi.getX2() < rstCi.getWordCi().getX1() // 등록상태 시작점 좌표 이전 텍스트
                    && addrLineCnt > 0 ) { // 주소라인
                        if(addrLineCnt == 1){
                            Matcher addrDateMatch = Pattern.compile(OcrUtils.dateRegEx1).matcher(inferText);
                            if(addrDateMatch.find()){
                                // 날짜 형식이고 주소 첫리인일경우
                                tempAddrDt = addrDateMatch.group();
                                // 주소 박스 끝라인 셋팅
                                tempAddrCi.setX2(currentCi.getX2());
                                tempAddrCi.setY2(currentCi.getY2());
                                tempAddrCi.setX3(currentCi.getX3());
                                tempAddrCi.setY3(currentCi.getY3());
                            }
                        } else if(addrLineCnt == 2) {
                            changeResonLine = inferText; 
                            tempAddrCi.setX3(currentCi.getX3());
                            tempAddrCi.setY3(currentCi.getY3());
                            tempAddrCi.setY4(currentCi.getY4());
                        }
                }

                // 줄바꿈
                if(lineBreak){
                    // 주소처리
                    if(addrLineCnt == 1){
                        if(tempAddrDt.isEmpty()){
                            // 첫라인에 날짜 정보가 없으면 주소가 아니라고 판단. > 오류
                            addrLineCnt = 0;
                        } else {
                            tempAddr = addrLine;
                            addrLine = "";
                            addrLineCnt = 2;
                        }
                    } else if(addrLineCnt == 2){
                        // 주소 셋팅 2번째라인시
                        if(!changeResonLine.isEmpty()){
                            tempAddr += " " + addrLine;
                            if(!tempAddr.isEmpty() && !tempAddrDt.isEmpty()){
                                OcrAddrVo oav = new OcrAddrVo();
                                oav.setAddr(tempAddr);
                                oav.setAddrDate(tempAddrDt);
                                oav.setAddrCi(tempAddrCi);
                                addrList.add(oav);
                            }
                            addrLine = "";
                            changeResonLine = "";
                            tempAddr = "";
                            tempAddrDt = "";
                            addrLineCnt = 0;
                        }
                    }
                }
                
                // 기준 좌표 지정 START

                // '용도및목적' 좌표 정의
                if(!uptCi.isCompareEnd()){
                    OcrUtils.findWordCi(uptCi, inferText, currentCi);
                }

                // '성명(한자)' 좌표 정의
                if(!anmCi.isCompareEnd()){
                    OcrUtils.findWordCi(anmCi, inferText, currentCi);
                }

                // 주소 > '번호' 좌표 정의
                if(!numCi.isCompareEnd() && currentCi.getX2() <= anmCi.getWordCi().getX2() ){
                    OcrUtils.findWordCi(numCi, inferText, currentCi);
                }
                
                // 주소 > '변동사유' 좌표 정의
                if(!acrCi.isCompareEnd()){
                    OcrUtils.findWordCi(acrCi, inferText, currentCi);
                }

                // 주소 > '등록상태' 좌표 정의
                if(!rstCi.isCompareEnd()){
                    OcrUtils.findWordCi(rstCi, inferText, currentCi);
                }

                // 기준 좌표 지정 END
            }

            if(i == 0){
                // 첫페이지 번호 ci  - 계산값 노출 기준
                ojc.setNumCi(numCi.getWordCi());
            }

            // 추출 기준 정보
            opv.setUptCi(uptCi);
            opv.setAnmCi(anmCi);
            opv.setAcrCi(acrCi);
            opv.setNumCi(numCi);
            opv.setRstCi(rstCi);

            // 페이지 셋팅
            opv.setBoxList(boxList);
            opv.setAddrList(addrList);
            // 초본 셋팅
            ojc.getPageList().add(opv);
            ojc.getAddrList().addAll(addrList);
		} // for images

        analyzeAddr(ojc);

        // 추출 정보
        result.put("chobon", ojc); 

        return result;
    }

   /**
	 * 추출 주소 분석
	 * 
     * @param OcrJuminChobonVo
     * @throws ParseException
     * @throws IOException
     * @see 거주 시작일
     * @see 마지막 거주 이전일
     * @see 거주 총 일수
     * @see 연속 거주 총 일수
     * @see 거주 기간, 거주 주소
     */
    public void analyzeAddr(OcrJuminChobonVo ojc) throws java.text.ParseException, IOException{
        int day = 0; // 거주 일수
        int conDay = 0; // 연속 거주 일수
        String residenceFirst = ""; // 최초 거주 시작일
		String residenceStart = ""; // 연속 거주 시작일
        String residenceLast = ""; // 거주 마지막 이전일
        String residDt = ""; // 기준주소 거주 일자
        boolean residYn = false; // 기준주소 거주 여부
        String currentAddr = ""; // 마지막 주소 현거주지

		// 추출 기준 주소 조회

        List<OcrAddrVo> addrList = new ArrayList<OcrAddrVo>();
        for(int p=0; p < ojc.getPageList().size(); p++){
            OcrPageVo opv = ojc.getPageList().get(p);

            for(int i=0; i < opv.getAddrList().size(); i++){
                OcrAddrVo oav = opv.getAddrList().get(i);
                // 마지막 주소를 현주소로 셋팅
                if(p == ojc.getPageList().size() - 1 && i == opv.getAddrList().size()-1){
                    currentAddr = oav.getAddr();
                }

                if(oav.getAddr().startsWith(ojc.getBsnsLocationName())){
                    // 기준주소 주소 리스트업
                    addrList.add(oav);
                    // 기준주소 주소 박스 처리
                    opv.getBoxList().add(oav.getAddrCi());
                    // 기준주소 연속 거주 시작 일자가 초기화 되어있으면 셋팅
                    if(residenceStart.isEmpty()){
                        residenceStart = oav.getAddrDate();
                    }
                    // 거주일 계산
                    if(residDt.isEmpty()){
                        // 최초 기준주소 거주 시작일
                        residDt = oav.getAddrDate();
                        residenceFirst = oav.getAddrDate();
                        residenceLast = oav.getAddrDate();
                        opv.getBoxList().add(oav.getAddrCi());
                        residYn = true;
                    } else {
                        if(residYn){
                            // 기준주소에서 기준주소 거주 이전 기간 산정
                            int addrTerm = OcrUtils.dateTermDays(residDt, oav.getAddrDate(), dateFormat);
                            day += addrTerm;
                            oav.getAddrCi().setText(Integer.toString(addrTerm) + "일");
                            residDt = oav.getAddrDate();
                        } else{
                            // 다른지역에서 기준주소 거주 이전
                            oav.getAddrCi().setText("0일");
                            opv.getBoxList().add(oav.getAddrCi());
                            residDt = oav.getAddrDate();
                            residYn = true;
                        }
                        
                        residenceLast = oav.getAddrDate();
                    }
                } else {
                    if(residYn){
                        // 기준주소 다른지역 거주 이전
                        day += OcrUtils.dateTermDays(residDt, oav.getAddrDate(), dateFormat);
                        residDt = oav.getAddrDate();
                        residYn = false;
                        residenceStart = "";
                    }
                }
            }
        }

        if(residYn){
            // 기준주소 거주중일경우 현재 날짜 포함 거주기간 포함, 현재 날짜기준 정의 필요
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String currentDt = formatter.format(date);
            day += OcrUtils.dateTermDays(residDt, currentDt, dateFormat);
            conDay = OcrUtils.dateTermDays(residenceStart, currentDt, dateFormat);
        }
        // 기준주소 거주기간 계산값
        String sumText = ojc.getBsnsLocationName()+ " 연속거주일 : " + Integer.toString(conDay)+ "일, " 
            + ojc.getBsnsLocationName() +" 합산거주일 : " + Integer.toString(day)+"일";
        // 첫페이지 번호 영역 상단에 위치
        OcrCiVo sumCi = new OcrCiVo();
        sumCi.setX2(ojc.getNumCi().getX1());
        sumCi.setY2(ojc.getNumCi().getY1() - 100);
        sumCi.setText(sumText);
        ojc.setSumCi(sumCi);

		ojc.setCurrentAddr(currentAddr);
		ojc.setResidenceFirst(residenceFirst);
		ojc.setResidenceStart(residenceStart);
		ojc.setResidenceLast(residenceLast);
		ojc.setContinueDay(conDay);
		ojc.setContinueYmd(OcrUtils.convertDayToYMD(conDay));
		ojc.setTotalDay(day);
		ojc.setTotalYmd(OcrUtils.convertDayToYMD(day));
		ojc.setSelAddrList(addrList);
    }
}
