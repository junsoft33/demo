package com.example.demo.ocr.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 주민등록초본/등본 OCR 데이터 분석 페이지 정보 ANALYSIS
 * @version 1.0
 * @author lcg
 */
@Getter
@Setter
public class OcrPageVo {
    // 페이지 번호
    int page = 0;

    // ocr 파일 넓이
    int fileWidth = 0;

    // ocr 파일 높이
    int fileHeight = 0;

    // 추출 데이터 박스 
    List<OcrCiVo> boxList = new ArrayList<>();

    // 페이지별 주소, 날짜, 좌표
    List<OcrAddrVo> addrList = new ArrayList<OcrAddrVo>();

    //'용도및목적' 좌표
    OcrWordCiVo uptCi = null;

    //주소 > '변동사유' 좌표
    OcrWordCiVo acrCi = null;

    //'현주소:' 좌표 - 등본
    OcrWordCiVo nadCi = null;

    // 세대원 '번호' 좌표
    OcrWordCiVo numCi = null;

    //'주민등록번호' 좌표
    OcrWordCiVo jrnCi = null;

    //'이하여백' 좌표 - 등본
    OcrWordCiVo bmnCi = null;

    //'성명(한자)' 좌표 - 초본
    OcrWordCiVo anmCi = null;

    //'등록상태' 좌표 - 초본
    OcrWordCiVo rstCi = null;
}
