package com.example.demo.ocr.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * OCR 주민번호 정보
 * @version 1.0
 * @author lcg
 */
@Getter
@Setter
public class OcrJuminVo {
    String juminRegNo = "";
    String juminRegNoAst = "";
    String birth = "";
    String gender = "";
    String name = "";
    String relation = "";
}
