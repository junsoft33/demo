package com.example.demo.ocr.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 주민등록초본 OCR 정보
 * @version 1.0
 * @author lcg
 */
@Getter
@Setter
@ToString
public class OcrJuminChobonVo {
    // OCR 데이터 분석 페이지 리스트
    List<OcrPageVo> pageList = new ArrayList<>();
    // 번호 ci 정보 - 계산값 작성기준 위치
    OcrCiVo numCi = new OcrCiVo();
    // 합산정보 노출 ci 정보
    OcrCiVo sumCi = new OcrCiVo();
    // 발급일
    String issueDt="";
    // 이름
    String issueNm="";
    // 주민등록번호
    String juminRegNo="";
    // 현 거주지
    String currentAddr="";
    // 기준주소 
    String bsnsLocationName;
	// 최초 거주 시작일
	String residenceFirst;
	// 연속 거주 시작일
	String residenceStart;
	// 마지막 거주 이전일
	String residenceLast;
	// 연속 거주일수
	int continueDay;
	// 연속거주년월일
	String continueYmd;
	// 합산 거주일수
	int totalDay;
	// 합산거주년월일
	String totalYmd;
    // 전체 주소,날짜리스트
    List<OcrAddrVo> addrList = new ArrayList<OcrAddrVo>();
    // 기준 주소,날짜리스트
    List<OcrAddrVo> selAddrList = new ArrayList<OcrAddrVo>();
}
