package com.example.demo.ocr.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 주민등록등본 OCR 정보
 * @version 1.0
 * @author lcg
 */
@Getter
@Setter
public class OcrJuminDeungbonVo {
    // OCR 데이터 분석 페이지 리스트
    List<OcrPageVo> pageList = new ArrayList<OcrPageVo>();

    //신청인
    String issueNm = "";

    //발급일
    String issueDt = "";

    //지원자
    String applyNm = "";

    //주민등록번호
    String juminRegNo = "";

    //현 거주지
    String address = "";
    
    //구성원 리스트
    List<DeungbonRelationVo> familyList = new ArrayList<DeungbonRelationVo>();
}
