package com.example.demo.ocr.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 주민등록 등본 가족관계 정보
 * @version 1.0
 * @author lcg
 */
@Getter
@Setter
public class DeungbonRelationVo {
    String juminRegNo = "";
    String juminRegNoAst = "";
    String birthYmd = "";
    String gender = "";
    String name = "";
    String relation = "";
}
