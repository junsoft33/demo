package com.example.demo.config;

public class Constant {
    // public final static String JSON_DIR_ROOT = "D:\\sample";
    public final static String JSON_DIR_ROOT = "D:\\advance";
    // NAVER CLOVA OCR TYPE
    public final static String OCR_GENERAL = "general";
    public final static String OCR_TEMPLATE = "template";
    public final static String OCR_DOCUMENT = "document";

    // OCR DOCUMENT TYPE
    public final static String JUMIN_CHOBON = "juminChobon"; // 주민등록초본
    public final static String JUMIN_DEUNGBON = "juminDeungbon"; // 주민등록등본
    public final static String HIEALC = "hiealc"; // 건강보험 자격득실 확인서
    public final static String RECEIPT = "receipt"; // 영수증
    public final static String BANKBOOK = "bankbook"; // 통장 사본
    public final static String TOEIC_STARE = "toeicStare";
    public final static String TOEIC_SCORE = "toeicScore";
    public final static String TOEIC_RECORD = "toeicRecord";
    public final static String TOEIC_RECEIPT = "toeicReceipt";
}
