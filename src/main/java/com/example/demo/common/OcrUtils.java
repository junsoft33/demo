package com.example.demo.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;

import com.example.demo.ocr.vo.DeungbonRelationVo;
import com.example.demo.ocr.vo.OcrCiVo;
import com.example.demo.ocr.vo.OcrWordCiVo;

@Component
public class OcrUtils {

    // OCR 정규식
    public final static String numRegEx = "^[0-9]+$"; //숫자일경우
    public final static String juminNoRegEx1 = "\\d{6}\\-[1-4](\\d{6}|\\*{6})"; //주민번호 111111-1111111
    public final static String juminNoRegEx2 = "\\d{6}\\-[1-4](\\d*|\\*+)"; //주민번호2 111111-1******
    public final static String dateRegEx1 = "(19|20|21)\\d{2}\\-((10|11|12)|(0?(\\d)))\\-(30|31|((0|1|2)?\\d))"; // 날짜일경우 ****-**-**
    public final static String dateRegEx2 = "(19|20|21)\\d{2}년\\s?((10|11|12)|(0?(\\d)))월\\s?(30|31|((0|1|2)?\\d))일"; // 날짜일경우 ****년 **월 **일
    
    public final static String[] sidoArray = {"서울특별","인천광역","대전광역","세종특별","부산광역","대구광역","울산광역","광주광역","경기","강원","충청","경상","전라","제주"};

    public static JSONArray ocrImageToJsonArray(String jonsStr) throws Exception{
        JSONArray jsonArray = null;
        if(!jonsStr.isEmpty()){
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(jonsStr);
            JSONObject jsonObj = (JSONObject) obj;
            // 추출 이미지 리스트
            jsonArray = (JSONArray)jsonObj.get("images");
        }
        return jsonArray;
    }


    /**
     * 문자 좌표 JSONObject > Map<String,Long>
     * 1-좌상, 2-우상, 3-우하, 4-좌하
     * 
     * @param JSONObject - 좌표 JSON DATA
     * @return Map<String,Long>
     * @throws Exception
     */
    public static Map<String,Long> getCoordinateMap(JSONObject boundingPoly) throws Exception{
        Map<String,Long> ci = new HashMap<>();
        JSONArray vertices = (JSONArray)boundingPoly.get("vertices");
        for(int i = 0; i < vertices.size(); i++){
            JSONObject vt = (JSONObject)vertices.get(i);
            ci.put("x"+(i+1), Math.round((Double) vt.get("x")));
            ci.put("y"+(i+1), Math.round((Double) vt.get("y")));
        }
        return ci;
    }

    /**
     * 문자 좌표 JSONObject > Map<String,Long>
     * 1-좌상, 2-우상, 3-우하, 4-좌하
     * 
     * @param JSONObject - 좌표 JSON DATA
     * @return OcrCiVo
     * @throws Exception
     */
    public static OcrCiVo getCoordinate(JSONObject boundingPoly) throws Exception{
        OcrCiVo ocv = new OcrCiVo();
        JSONArray vertices = (JSONArray)boundingPoly.get("vertices");
        for(int i = 0; i < vertices.size(); i++){
            JSONObject vt = (JSONObject)vertices.get(i);
            switch(i){
                case 0: 
                    ocv.setX1((int)Math.round((Double) vt.get("x")));
                    ocv.setY1((int)Math.round((Double) vt.get("y")));
                    break;
                case 1: 
                    ocv.setX2((int)Math.round((Double) vt.get("x")));
                    ocv.setY2((int)Math.round((Double) vt.get("y")));
                    break;
                case 2: 
                    ocv.setX3((int)Math.round((Double) vt.get("x")));
                    ocv.setY3((int)Math.round((Double) vt.get("y")));
                    break;
                case 3: 
                    ocv.setX4((int)Math.round((Double) vt.get("x")));
                    ocv.setY4((int)Math.round((Double) vt.get("y")));
                    break;
            }
        }
        return ocv;
    }

    /**
     * 문자열 CI 맵핑
     * 
     * @param OcrWordCiVo
     * @param String
     * @param Map<String,Long>
     */
    public static void findWordCi(OcrWordCiVo owc, String inferText, OcrCiVo currentCi){
        // 모든 공백 제거
        String reText = inferText.replaceAll(" ", "");
        if(!reText.isEmpty()){
            if(!owc.isCompareStart()){
                if(reText.equals(owc.getWord()) || OcrUtils.containsChar(owc.getWord(), reText)){
                    // 비교 문자와 모두 일치, 전체 포함 일치
                    owc.setCompareStart(true);
                    owc.setCompareEnd(true);
                    owc.setWordCi(currentCi);
                } else if(owc.getWord().startsWith(reText)){
                    // 비교 문자가 시작점일때
                    owc.setCompareStart(true);
                    owc.setTempWord(reText);
                    owc.setTempWordCi(currentCi);
                }
            } else {
                // 문자비교 1회 이상인경우
                String tempText = owc.getTempWord() + reText;
                if(tempText.equals(owc.getWord()) || OcrUtils.containsChar(owc.getWord(), tempText)){
                    // 최종문자열
                    OcrCiVo tempCi = owc.getTempWordCi();
                    tempCi.setX2(currentCi.getX2());
                    tempCi.setY2(currentCi.getY2());
                    tempCi.setX3(currentCi.getX3());
                    tempCi.setY3(currentCi.getY3());
                    owc.setCompareEnd(true);
                    owc.setWordCi(tempCi);
                } else if(owc.getWord().startsWith(tempText)){
                    owc.setTempWord(tempText);
                } else {
                    // 연속된 문자열이 아닌경우 초기화
                    owc.setCompareStart(false);
                    owc.setTempIdx(0);
                    owc.setTempWord("");
                }
            }
        }
    }

    /**
     * X좌표가 동일한지 확인, 보정범위 포함
     * 
     * @param orgCi - 비교 좌표
     * @param targetCi - 대상 좌표
     * @param correctRange - 좌표 보정 범위
     * @return boolean
     * @throws Exception
     */
    public static boolean rangeXMatched(Map<String,Long> orgCi, Map<String,Long> targetCi
        , int correctRange) throws Exception{
        boolean result = false;
        int orgx1 = orgCi.get("x1").intValue();
        int orgx2 = orgCi.get("x2").intValue();
        int targetx1 = targetCi.get("x1").intValue();
        int targetx2 = targetCi.get("x2").intValue();
        if(orgx1-correctRange <= targetx1 && orgx1+correctRange >= targetx1
            && orgx2-correctRange <= targetx2 && orgx2+correctRange >= targetx2){
                result = true;
        }

        return result;
    }

    /**
     * X1좌표가 동일한지 확인, 보정범위 포함
     * 
     * @param orgCi - 비교 좌표
     * @param targetCi - 대상 좌표
     * @param correctRange - 좌표 보정 범위
     * @return boolean
     * @throws Exception
     */
    public static boolean rangeXStartMatched(Map<String,Long> orgCi, Map<String,Long> targetCi
        , int correctRange) throws Exception{
        boolean result = false;
        int orgx1 = orgCi.get("x1").intValue();
        int targetx1 = targetCi.get("x1").intValue();
        if(orgx1-correctRange <= targetx1 && orgx1+correctRange >= targetx1){
                result = true;
        }

        return result;
    }

    /**
     * Y좌표가 동일한지 확인, 보정범위 포함
     * 
     * @param orgCi - 비교 좌표
     * @param targetCi - 대상 좌표
     * @param correctRange - 좌표 보정 범위
     * @return boolean
     * @throws Exception
     */
    public static boolean rangeYMatched(Map<String,Long> orgCi, Map<String,Long> targetCi
        , int correctRange) throws Exception{
        boolean result = false;
        int orgy1 = orgCi.get("y1").intValue();
        int orgy4 = orgCi.get("y4").intValue();
        int targety1 = targetCi.get("y1").intValue();
        int targety4 = targetCi.get("y4").intValue();
        if(orgy1-correctRange <= targety1 && orgy1+correctRange >= targety1
            && orgy4-correctRange <= targety4 && orgy4+correctRange >= targety4){
                result = true;
        }

        return result;
    }

    /**
     * 주민번호에서 생일 추출
     * 
     * @param String 
     * @return String
     */
    public static String convertJmToBirth(String juminRegNo){
        String birth = juminRegNo;
        String[] jms = juminRegNo.split("-");
        if(jms.length == 2){
            String year1 = "19";
            String gender = jms[1].substring(0,1);
            if("0".equals(gender) || "9".equals(gender)){
                year1 = "18";
            } else if("3".equals(gender) || "4".equals(gender) 
                || "7".equals(gender) || "8".equals(gender)){
                year1 = "20";
            }
            String year2 = jms[0].substring(0, 2);
            String month = jms[0].substring(2, 4);
            String day = jms[0].substring(4, 6);
            birth = year1 + year2 + "년 " + month + "월 " + day + "일";
        }
        return birth;
    }

    /**
     * 주민번호 아스테리크 처리
     * 주민번호 생년월일 
     * 주민번호 성별
     * 
     * @param String 
     * @return OcrJuminVo
     */
    public static DeungbonRelationVo convertJmToVo(String juminRegNo){
        DeungbonRelationVo ojv = new DeungbonRelationVo();
        ojv.setJuminRegNo(juminRegNo);
        String[] jms = juminRegNo.split("-");
        if(jms.length == 2){
            String year1 = "19";
            String gender = jms[1].substring(0,1);
            ojv.setJuminRegNoAst(jms[0] + "-" + gender + "******");
            switch(gender){
                case "0":
                    year1 = "18";
                    ojv.setGender("여");
                    break;
                case "1":
                    year1 = "19";
                    ojv.setGender("남");
                    break;
                case "2":
                    year1 = "19";
                    ojv.setGender("여");
                    break;
                case "3":
                    year1 = "20";
                    ojv.setGender("남");
                    break;
                case "4":
                    year1 = "20";
                    ojv.setGender("여");
                    break;
                case "5":
                    // 외국인
                    year1 = "19";
                    ojv.setGender("남");
                    break;
                case "6":
                    // 외국인
                    year1 = "19";
                    ojv.setGender("여");
                    break;
                case "7":
                    // 외국인
                    year1 = "20";
                    ojv.setGender("남");
                    break;
                case "8":
                    // 외국인
                    year1 = "20";
                    ojv.setGender("여");
                    break;
                case "9":
                    year1 = "18";
                    ojv.setGender("남");
                    break;
            }

            if("0".equals(gender) || "9".equals(gender)){
                year1 = "18";
            } else if("3".equals(gender) || "4".equals(gender) 
                || "7".equals(gender) || "8".equals(gender)){
                year1 = "20";
            }
            String year2 = jms[0].substring(0, 2);
            String month = jms[0].substring(2, 4);
            String day = jms[0].substring(4, 6);
            
            ojv.setBirthYmd(year1 + year2 + "년 " + month + "월 " + day + "일");
        }
        return ojv;
    }

   /**
     * 텍스트에 시도 존재 여부 확인   
     * 
     * @param String
     * @return Boolean
     */
    public static Boolean sidoContains(String text){
        for(String sido : sidoArray){
            if(text.contains(sido)){
                return true;
            }
        }
        return false;
    }

    /**
     * 텍스트를 시도로 시작하는지 확인 
     * 
     * @param String
     * @return Boolean
     */
    public static Boolean sidoStartsWith(String text){
        for(String sido : sidoArray){
            if(text.startsWith(sido)){
                return true;
            }
        }
        return false;
    }

    /**
     * 주소라인 시도 앞에 있는 문자 제외 처리
     * 
     * @param String
     * @return String
     */
    public static String removeSidoPostText(String text){
        for(String sido : sidoArray){
            if(text.contains(sido)){

                // 시도 앞에 텍스트 가 존재하여 제거 처리
                int gkIndex = text.indexOf(sido);
                text = text.substring(gkIndex);
            }
        }
        return text;
    }

    /**
     * 타켓 문자열에 해당 문자열이 모두 존재하는지 확인
     * 
     * @param String
     * @param String
     * @return boolean
     */
    public static boolean containsChar(String text, String target){
        for(int i=0; i<text.length(); i++){ 
            String charStr = Character.toString(text.charAt(i));
            if(!target.contains(charStr)){
                return false;
            }
        }
        return true;
    }

    /**
     * 사업유형
     * 
     * @param String
     * @return String
     */
    public static String bsnsTypeName(String code){
        String name = "";
        switch(code){
            case "01": name = "단일신청형";
                break;
            case "02": name = "복수신청형";
                break;
            case "03": name = "청년기본소득";
                break;
            case "04": name = "채용형";
                break;
            case "05": name = "시군지원형";
                break;
            case "06": name = "면접수당형";
                break;
        }
        return name;
    }

    /**
     * 문서유형
     * 
     * @param String
     * @return String
     */
    public static String aatAttTypeName(String code){
        String name = "";
        switch(code){
            case "1": name = "주민등록등본";
                break;
            case "2": name = "주민등록초본";
                break;
            case "3": name = "영수증";
                break;
            case "4": name = "사업자등록증";
                break;
        }
        return name;
    }

    /**
     * 자동분석 결과
     * 
     * @param String
     * @return String
     */
    public static String aatResultName(String code){
        String name = "";
        switch(code){
            case "R": name = "요청";
                break;
            case "C": name = "완료";
                break;
            case "E": name = "에러";
                break;
            case "D": name = "중복";
                break;
        }
        return name;
    }

    // 날짜 간격 일수 구하기
    public static int dateTermDays(String startDt, String endDt, String dateFormat) throws ParseException{
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        Date startDate = dateFormatter.parse(startDt);
        Date endDate = dateFormatter.parse(endDt);
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate); 
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate); 
        Long diffSec = (endCal.getTimeInMillis() - startCal.getTimeInMillis()) / 1000;
        Long diffDays = diffSec / (24*60*60); //일자수 차이
        return diffDays.intValue();
    }

    // 일수를 년월일로 표기
    public static String convertDayToYMD(int day){
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1*day);
        LocalDate startDate = cal.getTime().toInstant()   // Date -> Instant
        .atZone(ZoneId.systemDefault())  // Instant -> ZonedDateTime
        .toLocalDate();
        LocalDate currentDate = LocalDate.now();
        Period diff = Period.between(startDate, currentDate);
        return diff.getYears()+"년"+diff.getMonths()+"월"+diff.getDays()+"일";
    }
}
